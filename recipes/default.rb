#
# Cookbook Name:: gitlab-elastic
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
package 'curl'
package node['gitlab-elastic']['java_package']

release = node['gitlab-elastic']['release']

if release == '5'
  file '/etc/apt/sources.list.d/elasticsearch.list' do
    content "deb https://artifacts.elastic.co/packages/5.x/apt stable main\n"
    notifies :run, 'bash[add key]', :immediately
  end
else
  file '/etc/apt/sources.list.d/elasticsearch.list' do
    content "deb http://packages.elastic.co/elasticsearch/2.x/debian stable main\n"
    notifies :run, 'bash[add key]', :immediately
  end
end

bash 'add key' do
  code <<-EOH
    curl -s https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
    apt-get update
  EOH
  action :nothing
end

package 'elasticsearch' do
  action :install
  notifies :run, 'bash[init]', :immediately if release == '2'
  notifies :run, 'bash[install_plugin]', :delayed if release == '2'
end

package 'elasticsearch' do
  version node['gitlab-elastic']['version'] if node['gitlab-elastic']['version']
  action :lock
end

bash 'init' do
  code <<-EOH
    update-rc.d elasticsearch defaults
  EOH
  action :nothing
end

if node['gitlab-elastic']['prometheus_exporter']['install']
  pe_version = node['gitlab-elastic']['prometheus_exporter']['v2_version']
  dl_url = "https://github.com/vvanholl/elasticsearch-prometheus-exporter/releases/download/#{pe_version}/elasticsearch-prometheus-exporter-#{pe_version}.zip"

  if release == '5'
    pe_version = node['gitlab-elastic']['prometheus_exporter']['v5_version']
    dl_url = "https://distfiles.compuscene.net/elasticsearch/elasticsearch-prometheus-exporter-#{pe_version}.zip"
  end
  bash "install_prometheus_exporter_plugin" do
    code <<-EOH
      /usr/share/elasticsearch/bin/elasticsearch-plugin install -b #{dl_url}
    EOH
  end
end

bash "install_plugin" do
  code <<-EOH
    /usr/share/elasticsearch/bin/plugin install delete-by-query
  EOH
  action :nothing
end

if release == '5'
  template '/etc/elasticsearch/jvm.options' do
    owner 'root'
    group 'elasticsearch'
    mode '0660'
    notifies :restart, 'service[elasticsearch]'
  end
end

if release == '2'
  template '/etc/default/elasticsearch' do
    owner 'root'
    group 'root'
    mode '0644'
    notifies :restart, 'service[elasticsearch]'
  end
end

template '/etc/elasticsearch/elasticsearch.yml' do
  owner 'root'
  group 'elasticsearch'
  mode '0750'
  notifies :restart, 'service[elasticsearch]'
end

service 'elasticsearch' do
  action :enable
end

## Plugins
bash "install_x_pack" do
  code <<-EOH
      /usr/share/elasticsearch/bin/elasticsearch-plugin install x-pack --batch
  EOH
  not_if "/usr/share/elasticsearch/bin/elasticsearch-plugin list | grep 'x-pack'"
  only_if { node['gitlab-elastic']['plugin-x-pack'] }
  notifies :restart, 'service[elasticsearch]'
end
