# gitlab-elastic CHANGELOG

This file is used to list changes made in each version of the gitlab-elastic cookbook.

## 0.1.6
- fix plugin-x-pack

## 0.1.5
- use the right repository

## 0.1.4
- add elasticsearch 2 support as well

## 0.1.3
- Use elasticsearch 5

## 0.1.2
- update license

## 0.1.1
- change to use XFS and appropriate mount opts

## 0.1.0
- Jeroen Nijhof - Initial release of gitlab-elastic
